# Proj3-Flask Vocab
This project handles user input gathered in JavaScript through keyboard interaction. On keyup events, the input is checked by our Flask configuration to see if it matches any words in the list, and Flask sends json back to the JavaScript. Once the user has found the target number of words using the mixed letters, the game is over.

## Author: Alec Springel, aspring6@uoregon.edu ##

# Usage
First, build the flask image with:  
```docker build -t vocab .```  
To run the program run:  
```docker run -d -p 5000:5000 vocab``` and connect via a web browser